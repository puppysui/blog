import React, { useMemo } from 'react'
import useFetchList from '@/hooks/useFetchList'
import { HOME_PAGESIZE } from '@/utils/config'
import { translateMarkdown } from '@/utils'
import { Spin } from 'antd'
import ArticleList from './List'
import Pagination from '../../../components/Pagination'
import { useLocation } from 'react-router-dom'
import '@/styles/home.scss'

const Home = (props) => {
  const location = useLocation()

  const { dataList, loading, pagination } = useFetchList({
    requestUrl: '/article/list',
    queryParams: { pageSize: HOME_PAGESIZE },
    fetchDependence: [location.search],
  })

  const list = useMemo(() => {
    return [...dataList].map(item => {
      // const index = item.content.indexOf('<!--more-->')
      // item.content = translateMarkdown(item.content.slice(0, index))
      item.content = translateMarkdown(item.content)
      return item
    })
  }, [dataList])

  const handlePageChange = (page) => {
    document.querySelector('.app-main').scrollTop = 0
    pagination.onChange(page)
  }

  return (
    <Spin tip="Loading..." spinning={loading}>
      <div className="app-home">
        home
        <ArticleList list={list} />
        <Pagination {...pagination} onChange={handlePageChange} />
      </div>
    </Spin>
  )
}

export default Home
