import React, { Component } from 'react'
import { CommentOutlined, EyeOutlined } from '@ant-design/icons'

const ArticleList = (props) => {
  const { list } = props

  const jumpTo = () => {}

  return (
    <ul className="app-home-list">
      {list.map((item) => (
        <li key={item.id} className="app-home-list-item">
          <h1 className="title" onClick={() => jumpTo(item.id)}>
            {item.title}
          </h1>

          <div
            className="article-detail content"
            onClick={() => jumpTo(item.id)}
            dangerouslySetInnerHTML={{ __html: item.content }}
          />

          <ul className="list-item-others">
            <li className="posted-time">{item.createdAt.slice(0, 10)}</li>
            <li>
              <EyeOutlined />
              <span>{item.viewCount}</span>
            </li>
            <li>
              <CommentOutlined />
              {/* {JSON.stringify(item.comtents)} */}
              <span>255</span>
            </li>
            {/* 标签 */}
            <li>{JSON.stringify(item.tags.toString())}</li>
            {/* 目录 */}
            <li>{JSON.stringify(item.categories.toString())}</li>
          </ul>
        </li>
      ))}
    </ul>
  )
}

export default ArticleList
