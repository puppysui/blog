import { useRoutes } from 'react-router-dom';
import routes from '../src/routes';
import { useSelector } from 'react-redux'
import PublicComponent from '@/components/Public'

function App() {

  // const role = useSelector(state => state.user.info) // 相当于 connect(state => state.user.info)(App)

  return (
    <>
      {useRoutes(routes)}
      <PublicComponent />
    </>
  )
}

export default App;
