import axios from '@/utils/axios';
import React, { Component, useCallback, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom'
import { decodeQuery } from '@/utils'

export default function useFetchList({
    requestUrl = '', // 请求地址
    queryParams = null, // 请求参数
    withLoading = true,
    fetchDependence = [],
}) {

    const [dataList, setDataList] = useState([])
    const [loading, setLoading] = useState([])
    const [pagination, setPagination] = useState({ current: 1, pageSize: 10, total: 0 })

    const location = useLocation()
    const navigate = useNavigate()

    useEffect(() => {
        if (fetchDependence.length === 0) fetchWithLoading()
    }, [])

    useEffect(() => {
        if (fetchDependence.length > 0) {
            const params = decodeQuery(location.search)
            fetchWithLoading(params)
        }
    }, fetchDependence)

    function fetchWithLoading(params) {
        withLoading && setLoading(true)
        fetchDataList(params)
    }

    function fetchDataList(params) {
        // debugger
        const requestParams = {
            page: +pagination.current,
            pageSize: +pagination.pageSize,
            ...queryParams,
            ...params
        }

        axios.get(requestUrl, { params: requestParams }).then(res => {
            const pagination = {
                current: +requestParams.page,
                pageSize: +requestParams.pageSize,
                total: res.count
            }
            setPagination(pagination)
            setDataList(res.rows)
            withLoading && setLoading(false)
        }).catch(err => withLoading && setLoading(false))
    }

    const handlePageChange = useCallback(page => {
        // ...something
        console.log(location);
        console.log(page);
        const search = location.search.includes('page=')
            ? location.search.replace(/(page=)(\d+)/, `$1${page}`)
            : `?page=${page}`
        const jumpUrl = location.pathname + search

        navigate(jumpUrl)

    },
        [queryParams, location.pathname]

    )

    return {
        dataList,
        loading,
        pagination: {
            ...pagination,
            onChange: handlePageChange
        }
    }
}