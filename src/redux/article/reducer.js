import * as TYPES from '@/redux/types'
import { genertorColor } from '@/utils'

const defaultState = {
    categoryList: [],
    tagList: []
}

export default function articleRuducer(state = defaultState, action) {

    const { type, payload } = action
    switch (type) {
        case TYPES.ARTICLE_GET_TAG_LIST:
            const tagList = genertorColor(payload)
            return { ...state, tagList }
            break;

        default:
            return state
            break;
    }
}