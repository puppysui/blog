import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './rootReducers'

let storeEnhancers
storeEnhancers = compose(applyMiddleware(thunk))

const configureStore = (initialState = {}) => {
    const store = createStore(rootReducer, initialState, storeEnhancers)
    return store
}

export default configureStore()