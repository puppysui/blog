import { GithubOutlined, TwitterOutlined } from '@ant-design/icons'

export const HEADER_BLOG_NAME = 'puppsui blog'

// 后端接口地址
export const API_BASE_URL = 'http://121.5.121.181:6060'

// 侧边栏
export const SIDEBAR = {
    avatar: require('@/assets/images/avatar.jpeg'), // 侧边栏头像
    title: '标题', // 标题
    subTitle: '子标题', // 子标题
    // 个人主页
    homepages: {
      github: {
        link: 'https://github.com',
        icon: <GithubOutlined className="homepage-icon" />
      },
      twitter: {
        link: 'https://juejin.com',
        icon: <TwitterOutlined className="homepage-icon" style={{ color: '#1890ff' }} />
      }
    }
  }