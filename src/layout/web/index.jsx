import React from 'react';
// import { Outlet } from 'react-router-dom';
import { Layout, Row, Col } from 'antd';
import '@/styles/app.scss'

import Header from './header'
import SideBar from './sidebar'
import AppMain from './AppMain'

// 响应式
const siderLayout = { xxl: 4, xl: 5, lg: 5, sm: 0, xs: 0 }
const contentLayout = { xxl: 20, xl: 19, lg: 19, sm: 24, xs: 24 }

const WebLayout = () => {
  return (
    <Layout className="app-container">
      <Header />
      <Row className="app-content">
        <Col {...siderLayout}>
          <SideBar />
        </Col>
        <Col {...contentLayout}>
          <AppMain />
        </Col>
      </Row>
    </Layout>
  )
};

export default WebLayout;
