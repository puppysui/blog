import React from 'react'
import { HEADER_BLOG_NAME } from '@/config'

const HeaderLeft = props => {
    return <div>{HEADER_BLOG_NAME}</div>
}

export default HeaderLeft