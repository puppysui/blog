import React from 'react'
import { Layout, Row, Col } from 'antd'
import Left from './left'
import Right from './right'

const Header = Layout.Header

// 响应式
const leftLayout = { xxl: 4, xl: 5, lg: 5, sm: 4, xs: 24 }
const rightLayout = { xxl: 20, xl: 19, lg: 19, sm: 20, xs: 0 }

const WebHeader = () => (
  <Header className='app-header'>
    <Row>
      <Col {...leftLayout}>
        <Left />
      </Col>
      <Col {...rightLayout}>
        <Right />
      </Col>
    </Row>
  </Header>
)

export default WebHeader
