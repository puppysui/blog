import React from 'react'
import Navbar from './Navbar'
import Search from './Search'
import UserInfo from './UserInfo'

const HeaderRight = props => {
    return (
        <div className="header-right">
            <Search />
            <UserInfo />
            <Navbar />
        </div>
    )
}

export default HeaderRight