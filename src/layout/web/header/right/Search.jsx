import React, { useState } from 'react';
import { Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

const Search = () => {

    const [keyword, setKeyword] = useState('')

    const handleChange = e => {
        setKeyword(e.target.value)
    }

    const handlePressEnter = e => {
        e.target.blur()
    }

    const handleSubmit = e => {
        // ...
    }

    return (
        <div id="search-box">
            <SearchOutlined className="search-icon" />
            <Input
                className="search-input"
                onChange={handleChange}
                onBlur={handleSubmit}
                onPressEnter={handlePressEnter}
                placeholder="搜索文章"
                value={keyword}
            />
        </div>
    )
}

export default Search;