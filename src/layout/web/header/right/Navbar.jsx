/* eslint-disable no-unused-vars */
import React, { useState } from 'react'
import { Menu } from 'antd'
import { MailOutlined, AppstoreOutlined } from '@ant-design/icons';

const navList = [
    { key: 'home', title: '首页', icon: <MailOutlined /> },
    { key: 'edit', title: '归档', icon: <MailOutlined />, link: '/archives' },
    { key: 'folder', title: '分类', icon: <MailOutlined />, link: '/categories' },
    { key: 'user', title: '关于', icon: <MailOutlined />, link: '/about' },
]

const Navbar = props => {

    const [current, setCurrent] = useState('home')

    const handleClick = e => setCurrent(e.key)

    const { mode = 'horizontal' } = props

    return (
        <Menu mode={mode} selectedKeys={[current]} className="header-nav" >
            {navList.map(nav => (
                <Menu.Item key={nav.key}>
                    {nav.icon}
                    <span className='nav-text'>{nav.title}</span>
                </Menu.Item>
            ))}
        </Menu>
    )
}

export default Navbar