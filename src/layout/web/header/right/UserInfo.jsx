import React from 'react'
import { Button, Dropdown, Menu } from 'antd'

const UserInfo = () => {
  // const { username } =  {}

  const handleLogin = () => {}
  const handleRegister = () => {}

  const menu = (
    <Menu>
      <Menu.Item>1</Menu.Item>
      <Menu.Item>2</Menu.Item>
      <Menu.Item>3</Menu.Item>
    </Menu>
  )

  return (
    <div className="header-userInfo">
      {true ? (
        <Dropdown overlay={menu}>
          <Button>haha</Button>
        </Dropdown>
      ) : (
        <>
          <Button
            ghost
            type="primary"
            size="small"
            style={{ marginRight: 20 }}
            onClick={handleLogin}
          >
            登录
          </Button>
          <Button ghost type="danger" size="small" onClick={handleRegister}>
            注册
          </Button>
        </>
      )}
    </div>
  )
}

export default UserInfo
