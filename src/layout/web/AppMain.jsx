import React from 'react'
import { Outlet } from 'react-router-dom'

const AppMain = (props) => {
  return (
    <div className="app-main">
      <Outlet />
    </div>
  )
}

export default AppMain
