import React, { useEffect, useState } from 'react'
import { SIDEBAR } from '@/config'
import axios from '@/utils/axios'
import { useSelector } from 'react-redux'

// components
import { Link } from 'react-router-dom'
// import Href from '@/components/Href'
import { Icon, Divider, Tag } from 'antd'

import { Alert } from 'antd'
import { ANNOUNCEMENT } from '@/config'

import useFetchList from '@/hooks/useFetchList'

function SideBar(props) {
    const tagList = useSelector(state => state.article.tagList || [])

    const { dataList: articleList } = useFetchList({
        withLoading: false,
        requestUrl: '/article/list',
        queryParams: {
            order: 'viewCount DESC',
            page: 1,
            pageSize: 6
        }
    })

    return (
        <aside className="app-sidebar">
            <img src={SIDEBAR.avatar} className="avatar" alt="" />
            <h2 className="title">{SIDEBAR.title}</h2>
            <h5 className="sub-title">{SIDEBAR.subTitle}</h5>
            <ul className="home-pages">
                {Object.entries(SIDEBAR.homepages).map(([linkName, item]) => (
                    <li key={linkName}>
                        {item.icon}
                        <a href={item.link}>{linkName}</a>
                    </li>
                ))}
            </ul>

            {/* {ANNOUNCEMENT.enable && <Alert message={ANNOUNCEMENT.content} type='info' />} */}

            <Divider orientation="left">热门文章</Divider>
            <ul className="article-list">
                {articleList.map(item => (
                    <li key={item.id}>
                        <Link to={`/article/${item.id}`}>{item.title}</Link>
                    </li>
                ))}
            </ul>

            <Divider orientation="left">标签</Divider>
            <div className="tag-list">
                {tagList.map((tag, idx) => (
                    <Tag key={idx} color={tag.color}>
                        <Link to={`/tags/${tag.name}`}>{tag.name}</Link>
                    </Tag>
                ))}
            </div>
        </aside>
    )
}

export default SideBar
