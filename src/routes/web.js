import WebLayout from '@/layout/web'
import Home from '../views/web/home'

const route = {
    path: '/',
    name: 'home',
    element: <WebLayout />,
    children: [
        { index: true, element: <Home /> },
        { path: '1', element: <Home /> },
        { path: '3', element: <Home /> },
        { path: '4', element: <Home /> },
    ]
}

export default route
