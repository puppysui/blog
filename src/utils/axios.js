import axios from 'axios'
import { API_BASE_URL } from '@/config'

import { message } from 'antd'

const service = axios.create({
    baseURL: API_BASE_URL,
    // withCredentials: true, // send cookies when cross-domain requests
    timeout: 10000
})

let timer

// 拦截请求
// service.interceptors.request.use(
//     config => {
        
//     }
// )

// 拦截响应
service.interceptors.response.use(
    response => {
        return response.data
    },
    err => {
        clearTimeout(timer)
        timer = setTimeout(() => {
            if (err.response) {
                
            } else {
                message.error(err.message)
            }
        }, 200)
    }
)

export default service