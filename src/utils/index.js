import { marked } from 'marked'
import hljs from 'highlight.js'
import { COLOR_LIST } from '@/utils/config'

// 获取 url query 参数
export const decodeQuery = url => {
    const params = {}
    const paramsStr = url.replace(/\.*\?/, '') // a=1&b=2&c=&d=xxx&e
    paramsStr.split('&').forEach(v => {
        const d = v.split('=')
        if (d[1] && d[0]) params[d[0]] = d[1]
    })
    return params
}

// 转化 md 语法为 html
export const translateMarkdown = (plainText, isGuardXss = false) => {
    // const text = isGuardXss ? xss(plainText) : plainText
    return marked(plainText,
        {
            renderer: new marked.Renderer(),
            gfm: true, // 允许 github 标准的 markdown
            pedantic: false, // 不纠正原始模型任何的不良行为和错误（默认为false）
            sanitize: false, // 对输出进行过滤（清理），将忽略任何已经输入的html代码（标签）
            tables: true, // 允许支持表格语法（该选项要求 gfm 为 true）
            breaks: true, // 允许回车换行（该选项要求 gfm 为 true）
            smartLists: true, // 使用比原生 markdown 更时髦的列表
            smartypants: true, // 使用更为时髦的标点
            highlight: function (code) {
                /*eslint no-undef: "off"*/
                return hljs.highlightAuto(code).value
            }
        })
}

// 生成 color
export function genertorColor(list = [], colorList = COLOR_LIST) {
    const _list = [...list]
    _list.forEach((l, i) => {
        l.color = colorList[i]
    })
    return _list
}